# Scheduling

Provides a scheduling feature for fieldable entities & integrates well with Drupal's [Cache API](https://www.drupal.org/docs/8/api/cache-api/cache-api). Entities can be scheduled either with a date range or recurringly on specific timeslots on specific week days.

## Dependencies

- none